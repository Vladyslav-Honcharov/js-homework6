function filterBy() {
  let firstArg = ["hello", "world", 23, "23", null, true, {}];
  let secondArg = 45;
  let result = firstArg.filter(
    (parametr, index, array) => typeof parametr !== typeof secondArg
  );
  console.log(result);
}
filterBy();
